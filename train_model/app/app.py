"""
This is the implementation of data preparation for sklearn
"""

import os
import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

import pandas as pd
from datetime import datetime, timedelta
import numpy as np
from sklearn.ensemble import RandomForestRegressor
import pickle
from sklearn.model_selection import train_test_split
import lightgbm as lgb



__author__ = "### Author ###"

logger = XprLogger("train_model",level=logging.INFO)


class TrainModel(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="TrainModel")
        """ Initialize all the required constansts and data her """

        self.train_data = None

        self.fe_fp = "/data/dataset_with_features.xlsx"
        self.model = None


    # def rf(self):
    #     features = ['Province', 'Country', 'Lat', 'Long', 'Date', 'Confirmed', 'Month',
    #                 'Day', 'Day_Week', 'quarter', 'dayofyear', 'weekofyear',
    #                 'conf_rollmean_7', 'conf_rollstd_7', 'DateDiff']
    #
    #     #     train,test=train_test_split(data,7)
    #     y = self.train_data['Confirmed']
    #     #         np.
    #     np.squeeze(y, axis=0)
    #     # self.train_data.to_csv('fe.csv', index=None)
    #     x = self.train_data.drop(['Confirmed', 'Date', 'Country'], axis=1)
    #     print(x.columns)
    #     #         shuffling to be performed
    #     X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.00001)
    #
    #     self.model = RandomForestRegressor(n_estimators=50, n_jobs=-1, min_samples_leaf=1)
    #
    #     self.model.fit(X_train, y_train)



        #         return rf

        # predictions = rf.predict(X_test)

        # rmsle = RMSLError(y_test, predictions)

        # print(rmsle)

    def rf(self):
        #         features=['Province', 'Country', 'Lat', 'Long', 'Date', 'Confirmed', 'Month',
        #                    'Day', 'Day_Week', 'quarter', 'dayofyear', 'weekofyear',
        #                    'conf_rollmean_7', 'conf_rollstd_7','DateDiff']

        #         features=['Province', 'Country', 'Lat', 'Long', 'Date', 'Confirmed', 'Month',
        #                    'Day', 'Day_Week', 'quarter', 'dayofyear', 'weekofyear',
        #                    'conf_rollmean_7', 'conf_rollstd_7','DateDiff']

        y = self.train_data['Confirmed'].values

        X = self.train_data.loc[:, self.train_data.columns != 'Confirmed']

        X = self.train_data.drop(['Date','Country','Confirmed'], axis=1)

        feature_names = X.columns.tolist()

        categorical = ['Country_enc']

        print(feature_names)

        # self.train_data['Country'] = pd.to_numeric(self.train_data['Country'])



        X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.00001, random_state=42)

        # LightGBM dataset formatting
        lgtrain = lgb.Dataset(X_train, y_train,
                              feature_name=feature_names,
                              categorical_feature=categorical)
        params = {
            'boosting_type': 'gbdt',
            'objective': 'regression',
            'metric': 'rmse',
            'seed': 236,
            'num_leaves': 200,
            'max_depth': 24,
            'learning_rate': 0.1,
            'feature_fraction': 0.8,
            'verbosity': -1

        }

        self.model = lgb.train(
            params,
            lgtrain,
            num_boost_round=5000,
            valid_sets=[lgtrain],
            valid_names=["train"],
            early_stopping_rounds=500,
            verbose_eval=500
        )


    #         X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42)

    #         rf = RandomForestRegressor(n_estimators=50, n_jobs= -1, min_samples_leaf=1, random_state=17)

    #         rf.fit(X_train,y_train)

    #         predictions = rf.predict(X_test)

    #         rmsle = RMSLError(y_test,predictions)

    #         print(rmsle)

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        # print(self.fe_fp)
        self.train_data = pd.read_excel(self.fe_fp)

        self.train_data['Date'] = pd.to_datetime(self.train_data['Date'])

        self.rf()

        self.completed(True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===


        if (not (os.path.exists(self.OUTPUT_DIR) ) ):
            os.makedirs(self.OUTPUT_DIR)


        # self.train_data.to_excel(os.path.join('../data', 'feature_engg_file.xlsx'),index=None)
        # pickle.dump(self.model, open(os.path.join('../data', 'trained_model.sav'), 'wb'))

        self.train_data.to_excel(os.path.join(self.OUTPUT_DIR, 'feature_engg_file.xlsx'))
        pickle.dump(self.model, open(os.path.join(self.OUTPUT_DIR, 'trained_model.sav'), 'wb'))
        print("All steps done")

        super().completed(push_exp=push_exp)


    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = TrainModel()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
