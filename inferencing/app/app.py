"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger
from sklearn.preprocessing import LabelEncoder,OneHotEncoder

import pandas as pd
import os
import pickle


__author__ = "### Author ###"

logger = XprLogger("inferencing",level=logging.INFO)


class Inferencing(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """

        self.fe_filename = 'feature_engg_file.xlsx'
        self.fe_data = None
        self.output_path = None
        self.model_file_name = 'trained_model.sav'
        self.model = None

        self.final_prediction = None


    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """

        self.fe_data = pd.read_excel(os.path.join(model_path,self.fe_filename))
        self.model = pickle.load(open(os.path.join(model_path, self.model_file_name), 'rb'))

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        test_data= pd.read_excel(os.path.join("/inf_data",input_request["test_data_file"]))

        # test_data= pd.read_excel(os.path.join("",input_request["test_data_file"]))

        self.output_path = os.path.join("/inf_data",input_request["predictions_location"])

        # self.output_path = os.path.join("",input_request["predictions_location"])

        return test_data.Country.unique()

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        self.fe_data['Date'] = pd.to_datetime(self.fe_data['Date'])
        #         print(fe_data.dtypes)
        Date = self.fe_data['Date'].max()

        curr_date1 = []

        for i in range(1, 15):
            curr_date = pd.to_datetime(Date + pd.Timedelta(days=i))
            curr_date1.append(curr_date)

        new_date = pd.DataFrame(curr_date1)

        new_date.columns = ['Date']
        #         new_country = fe_data.Country.unique()
        t = self.fe_data.sort_values(['Country', 'Date']).groupby(['Country']).last()[
            ['conf_rollmean_7', 'conf_rollstd_7']].reset_index()

        #         new_c_mean_std =  fe_data.sort_values(['Country','Date']).groupby(['Country']).last()[['conf_rollmean_7','conf_rollstd_7']].reset_index()

        #         print(new_country)
        #         index = pd.MultiIndex.from_product([new_date.Date, new_country], names = ["Date", "Country"])

        index = pd.MultiIndex.from_product([new_date.Date, t.Country], names=["Date", "Country"])
        final = pd.DataFrame(index=index).reset_index()
        #         print(final)

        t1 = pd.merge(final, t, on='Country')

        t1['Month'] = t1['Date'].dt.month
        t1['Day'] = t1['Date'].dt.day
        t1['Day_Week'] = t1['Date'].dt.dayofweek
        t1['quarter'] = t1['Date'].dt.quarter
        t1['dayofyear'] = t1['Date'].dt.dayofyear
        t1['weekofyear'] = t1['Date'].dt.weekofyear

        min_date = self.fe_data['Date'].min()
        t1['DaysDiff'] = t1['Date'].apply(lambda x: x - min_date).dt.days

        #         print(df.head())
        print(t1)

        # t1 = pd.concat([t1, pd.get_dummies(t1['Country'])], axis=1)

        categorical = ['Country']

        label_encoder = LabelEncoder()

        for col in categorical:
            t1['Country_enc'] = label_encoder.fit_transform(t1[col])

        z = t1[['Date','Country']]



        x = t1.drop(['Date', 'Country'], axis=1)

        predictions = self.model.predict(x)

        predictions = predictions.reshape(-1,1)
        z['Confirmed'] = predictions

        z['Confirmed'] = abs(z['Confirmed'])

        self.final_prediction = z.copy()



    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        self.final_prediction.to_csv(os.path.join(self.output_path, 'predictions.csv'), index=None)

        return "File generated"


if __name__ == "__main__":
    pred = Inferencing()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="../data")# instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
